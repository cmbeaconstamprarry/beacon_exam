//
//  ViewController.m
//  VennDiagram
//
//  Created by tanaka.keisuke on 2015/02/25.
//  Copyright (c) 2015年 Classmethod. All rights reserved.
//

#import "ViewController.h"
@import CoreLocation;

static NSString *const kBeaconUUID1 = @"xxxxx";
static NSString *const kBeaconIdentifier1 = @"jp.classmethod.VennDiagram.1";
static CLBeaconMajorValue const kBeaconMajorValue1 = 1;
static CLBeaconMinorValue const kBeaconMinorValue1 = 1;

static NSString *const kBeaconUUID2 = @"xxxxx";
static NSString *const kBeaconIdentifier2 = @"jp.classmethod.VennDiagram.2";
static CLBeaconMajorValue const kBeaconMajorValue2 = 2;
static CLBeaconMinorValue const kBeaconMinorValue2 = 2;

static NSString *const kBeaconUUID3 = @"xxxxx";
static NSString *const kBeaconIdentifier3 = @"jp.classmethod.VennDiagram.3";
static CLBeaconMajorValue const kBeaconMajorValue3 = 3;
static CLBeaconMinorValue const kBeaconMinorValue3 = 3;

@interface ViewController () <CLLocationManagerDelegate>

@property (nonatomic) CLLocationManager *locationManager;
@property (nonatomic) BOOL isNearBeacon1;
@property (nonatomic) BOOL isNearBeacon2;
@property (nonatomic) BOOL isNearBeacon3;

@end

@implementation ViewController

#pragma mark - Life cycle

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self setupBeacons];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager
        didRangeBeacons:(NSArray *)beacons
               inRegion:(CLBeaconRegion *)region
{
    CLBeacon *beacon = beacons.firstObject;
    NSLog(@"uuid %@", beacon.proximityUUID);
    NSLog(@"major %@", beacon.major);
    NSLog(@"minor %@", beacon.minor);

    switch (beacon.proximity) {
        case CLProximityImmediate:
            NSLog(@"CLProximityImmediate");
            [self setIsNearBeaconWithValue:NO major:beacon.major minor:beacon.minor];
            break;
        case CLProximityNear:
            NSLog(@"CLProximityNear");
            [self setIsNearBeaconWithValue:YES major:beacon.major minor:beacon.minor];
            break;
        case CLProximityFar:
            NSLog(@"CLProximityFar");
            [self setIsNearBeaconWithValue:NO major:beacon.major minor:beacon.minor];
            break;
        case CLProximityUnknown:
            NSLog(@"CLProximityUnknown");
            [self setIsNearBeaconWithValue:NO major:beacon.major minor:beacon.minor];
            break;
    }

    [self changeBackgroundColor];
}

#pragma mark - Private

- (void)setupBeacons
{
    if ([CLLocationManager isMonitoringAvailableForClass:[CLBeaconRegion class]]) {
        self.locationManager = [CLLocationManager new];
        self.locationManager.delegate = self;

        if ([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
            [self.locationManager requestAlwaysAuthorization];
        }

        for (NSDictionary *beaconInfo in [self beaconsInformation]) {
            CLBeaconRegion *beaconRegion =
            [[CLBeaconRegion alloc] initWithProximityUUID:[[NSUUID alloc] initWithUUIDString:beaconInfo[@"uuid"]]
                                                    major:[beaconInfo[@"major"] integerValue]
                                                    minor:[beaconInfo[@"minor"] integerValue]
                                               identifier:beaconInfo[@"identifier"]];
            [self.locationManager startRangingBeaconsInRegion:beaconRegion];
        }
    }
}

- (NSArray *)beaconsInformation
{
    NSDictionary *info1 = @{ @"uuid": kBeaconUUID1, @"major": @(kBeaconMajorValue1), @"minor": @(kBeaconMinorValue1), @"identifier": kBeaconIdentifier1 };
    NSDictionary *info2 = @{ @"uuid": kBeaconUUID2, @"major": @(kBeaconMajorValue2), @"minor": @(kBeaconMinorValue2), @"identifier": kBeaconIdentifier2 };
    NSDictionary *info3 = @{ @"uuid": kBeaconUUID3, @"major": @(kBeaconMajorValue3), @"minor": @(kBeaconMinorValue3), @"identifier": kBeaconIdentifier3 };

    return @[ info1, info2, info3 ];
}

- (void)setIsNearBeaconWithValue:(BOOL)value major:(NSNumber *)major minor:(NSNumber *)minor
{
    if (([major intValue] == kBeaconMajorValue1) && ([minor intValue] == kBeaconMinorValue1)) {
        self.isNearBeacon1 = value;
    } else if (([major intValue] == kBeaconMajorValue2) && ([minor intValue] == kBeaconMinorValue2)) {
        self.isNearBeacon2 = value;
    } else if (([major intValue] == kBeaconMajorValue3) && ([minor intValue] == kBeaconMinorValue3)) {
        self.isNearBeacon3 = value;
    }
}

- (void)changeBackgroundColor
{
    NSUInteger nearCount = self.isNearBeacon1 + self.isNearBeacon2 + self.isNearBeacon3;

    switch (nearCount) {
        case 1: self.view.backgroundColor = [UIColor cyanColor];    break;
        case 2: self.view.backgroundColor = [UIColor yellowColor];  break;
        case 3: self.view.backgroundColor = [UIColor magentaColor]; break;
        default: self.view.backgroundColor = [UIColor whiteColor];  break;
    }
}

@end
